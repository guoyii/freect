/*
Copyright © 2022 NAME HERE <EMAIL ADDRESS>

*/
package main

import "gitlab.com/freect/freect/cmd/fct-anonymize/cmd"

func main() {
	cmd.Execute()
}
